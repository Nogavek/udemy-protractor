var CommonElements = require('../pageObj/common.po.js');
var CartPage = require('../pageObj/cart.po.js');

var commonElements = new CommonElements();
var cartPage = new CartPage();

describe('Cart page:', function(){
    var until = protractor.ExpectedConditions;
    
    
    beforeEach(function(){
        browser.get(browser.params.url);
            
        
    });
    
    afterEach(function(){
        browser.manage().deleteAllCookies();
    });
    
    it('Should navigate to the cart and verify that it is empty', function(){
        
        commonElements.cartIcon.click();
        
        expect(cartPage.emptyCartMessage.getText()).toEqual(cartPage.emptyCartNotification);
        
        
    });
});