var HomePage = require('../pageObj/home.po.js');

describe('Home page: login', function(){
    var homePage = new HomePage();
    var until = protractor.ExpectedConditions;


    beforeEach(function(){
         browser.get(browser.params.url);

    });

    afterEach(function(){
        browser.manage().deleteAllCookies();
    });



    it('1. Should have a title', function(){

       expect(browser.getTitle()).toEqual(homePage.pageTitle);
    });

    it('2. Should try to sign in and verify error appear', function(){

        homePage.goToSignIn();
        browser.wait(until.visibilityOf(homePage.userPasswordField),5000);

        homePage.userNameField.sendKeys('test@test.com');
        homePage.userPasswordField.sendKeys('password');
        homePage.rememberMeCheck.click();
        homePage.signIn.click();

        browser.wait(until.visibilityOf(homePage.passwordErrorMessage),5000);
        expect(homePage.passwordErrorMessage.getText()).toEqual(homePage.passwordExistText);
    });

    it('3. Should try to sign in and verify that password cant be blank error appear', function(){

        homePage.goToSignIn();
        browser.wait(until.visibilityOf(homePage.userPasswordField),5000);

        homePage.userNameField.sendKeys('test@test.com');
        homePage.userPasswordField.sendKeys('');
        homePage.rememberMeCheck.click();
        homePage.signIn.click();

        browser.wait(until.visibilityOf(homePage.passwordErrorMessage),5000);
        expect(homePage.passwordErrorMessage.getText()).toEqual(homePage.passwordBlankText);
    });

    it('4. Should try to sign in and verify that email cant be blank error appear', function(){

        homePage.goToSignIn();
        browser.wait(until.visibilityOf(homePage.userPasswordField),5000);

        homePage.userNameField.sendKeys('');
        homePage.userPasswordField.sendKeys('password');
        homePage.rememberMeCheck.click();
        homePage.signIn.click();

        browser.wait(until.visibilityOf(homePage.passwordErrorMessage),5000);
        expect(homePage.emailErrorMessage.getText()).toEqual(homePage.emailBlankText);
    });

    it('5. Should register a new user', function(){

        homePage.goToRegister()
        browser.wait(until.visibilityOf(homePage.enterPasswordField),5000);
        homePage.doRegister()
    });

});
