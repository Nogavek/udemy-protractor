
var faker = require('faker');


var HomePage = function(){


    //faker data
    this.randomFirstName = faker.name.firstName();
    this.email = faker.internet.email();
    this.password = faker.internet.password();

    //text
    this.pageTitle = 'Etsy - Shop for handmade, vintage, custom, and unique gifts for everyone';
    this.passwordExistText = 'Password was incorrect.';
    this.passwordBlankText = "Can't be blank.";
    this.emailBlankText = "Can't be blank.";

    //id
    this.signInButton = element(by.id('sign-in'));
    this.userNameField = element(by.id('join_neu_email_field'));
    this.userPasswordField = element(by.id('join_neu_password_field'));
    this.emailErrorMessage = element(by.id('aria-join_neu_email_field-error'));
    this.passwordErrorMessage = element(by.id('aria-join_neu_password_field-error'));
    //register
    this.openRegisterForm = element(by.id('register'));
    this.enterEmailField = element(by.id('join_neu_email_field'));
    this.enterFirstNameField = element(by.id('join_neu_first_name_field'));
    this.enterPasswordField = element(by.id('join_neu_password_field'));

    //classname
    this.rememberMeCheck = element(by.className('checkbox-custom checkbox-large'));

    //name
    this.signIn = element(by.name('submit_attempt'));

    //xpath
    this.registerButton = element(by.xpath('//*[@id="join-neu-form"]/div[1]/div/div[5]/div/button'));

    this.goToSignIn = function(){
        this.signInButton.click();
    }

    this.goToRegister = function(){
        this.openRegisterForm.click();
        
    }

    // this.doSignIN = function(){
    //
    // }

    this.doRegister = function() {

      this.enterEmailField.sendKeys(this.email);
      this.enterFirstNameField.sendKeys(this.randomFirstName);
      this.enterPasswordField.sendKeys(this.password);
      this.registerButton.click();
    }

}

module.exports = HomePage;
