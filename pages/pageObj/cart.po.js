var CartPage = function(){
    
    this.emptyCartMessage = element(by.xpath('.//*[@id="newempty"]/div/h2'));
    
    this.emptyCartNotification = 'Your cart is empty.';
    
}

module.exports = CartPage; 