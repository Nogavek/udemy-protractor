var SearchPage = require('./search.po.js');
describe('Cart page:', function(){
    var searchPage = new SearchPage();
    var until = protractor.ExpectedConditions;


    beforeEach(function(){
         browser.get(browser.params.url);

    });

    afterEach(function(){
        browser.manage().deleteAllCookies();
    });

    it('Should search an Iphone case', function(){
           searchPage.searchBar.sendKeys('iPhone Assorted Models Case');
           browser.wait(until.elementToBeClickable(searchPage.searchButton),5000);
           searchPage.searchButton.click();
           browser.wait(until.visibilityOf(searchPage.searchItem),5000);

           searchPage.searchItem.click();


          browser.getAllWindowHandles().then(function(handles){
            browser.switchTo().window(handles[1]).then(function(){
              return searchPage.addToCartButton.click();
            });
          });




          browser.wait(until.visibilityOf(searchPage.selectDeviceErrorText),5000);
          expect(searchPage.selectDeviceErrorText.getText()).toEqual('Please select an option');


    });
});
