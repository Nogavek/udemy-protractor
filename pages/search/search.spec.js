var SearchPage = require('./search.po.js');
describe('Cart page:', function(){
    var searchPage = new SearchPage();
    var until = protractor.ExpectedConditions;


    beforeEach(function(){
         browser.get(browser.params.url);

    });

    afterEach(function(){
        browser.manage().deleteAllCookies();
    });

    it('Should search an Iphone case', function(done){
           searchPage.searchBar.sendKeys('iPhone Assorted Models Case')
           .then(function(){//Using promises
            return browser.wait(until.elementToBeClickable(searchPage.searchButton),5000);
           })
           .then(function(){
              return searchPage.searchButton.click();
           })
           .then(function(){
              return browser.wait(until.visibilityOf(searchPage.searchItem),5000);
           })
           .then(function(){
             return searchPage.searchItem.click();
           })
           .then(function(){
            return  browser.getAllWindowHandles().then(function(handles){
                browser.switchTo().window(handles[1]).then(function(){
                  return searchPage.addToCartButton.click();
                })
              })
           })
           .then(function(){
              return expect(searchPage.selectDeviceErrorText.getText()).toEqual('Please select an option');
           })
           .then(done);

          //   browser.wait(until.elementToBeClickable(searchPage.searchButton),5000);
          //   searchPage.searchButton.click();
          //   browser.wait(until.visibilityOf(searchPage.searchItem),5000);
          //   searchPage.searchItem.click();
          //
          //   browser.getAllWindowHandles().then(function(handles){
          //       browser.switchTo().window(handles[1]).then(function(){
          //         return searchPage.addToCartButton.click();
          //       });
          //   });
          //
          //  searchPage.addToCartButton.click();
          //  browser.wait(until.visibilityOf(searchPage.selectDeviceErrorText),5000);
          //  expect(searchPage.selectDeviceErrorText.getText()).toEqual('Please select an option');


    });
});
