var SearchPage = function(){
  this.searchBar = element(by.id('search-query'));
  this.searchButton = element(by.buttonText('Search'));
  this.searchItem = element(by.xpath('//*[@id="reorderable-listing-results"]/li/div[1]'));
  this.linkText = element(by.partialLinkText('PrimedesginArt'));
  this.addToCartButton = element(by.xpath('//*[@id="listing-page-cart"]/div[1]/div/div[3]/div/form/button[1]'));
  this.selectDeviceErrorText = element(by.className("has-error-msg text-red is-visible"));

}

module.exports = SearchPage;
