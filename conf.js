exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['./pages/**/*.spec.js'],
    suites: {
        all: "./pages/**/*.spec.js",
        home: "./pages/**/home.spec.js",
        cart: "./pages/**/cart.spec.js",
        search: "./pages/**/search.spec.js"
    },
    params:{
        url: 'https://www.etsy.com/'
    },

    onPrepare: function(){
        browser.ignoreSynchronization = true;
    },
    /*browserName: 'chrome',
    directConnect: true*/
  /*  multiCapabilities:[
      {
        'browserName' : 'chrome'
      },
      {
        'browserName' : 'firefox'
      }
    ]*/

}
